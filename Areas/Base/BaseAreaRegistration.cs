﻿using System.Web.Mvc;

namespace Auction.Areas.Base {
	public class BaseAreaRegistration : AreaRegistration {

		public override string AreaName {
			get {
				return "Base";
			}
		}

		public override void RegisterArea( AreaRegistrationContext context ) {

			context.MapRoute(
				"Base_product",
				"Base/{action}/{title}",
				new { action = "Index", controller = "Base", id = UrlParameter.Optional }
				);

			context.MapRoute(
				"Base_default",
				"Base/{action}/{id}",
				new { action = "Index", controller = "Base", id = UrlParameter.Optional }
			);


		}
	}
}

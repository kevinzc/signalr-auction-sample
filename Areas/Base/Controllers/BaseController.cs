﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Auction.Areas.Base.Hubs;
using Auction.Repositories;
using Microsoft.AspNet.SignalR;

namespace Auction.Areas.Base.Controllers {

	public class BaseController : Controller {
		//
		// GET: /Base/Base/

		public ActionResult Index() {

			var products = ProductStaticRepository.GetAll();

			return View( products );
		}

		public ActionResult Product( string title ) {

			var product = ProductStaticRepository.Get( title );

			if ( product == null ) {
				return HttpNotFound();
			}

			return View( product );
		}

		[HttpGet]
		public ActionResult Edit( string title ) {
			var product = ProductStaticRepository.Get( title );

			return View( product );
		}

		[HttpPost]
		public ActionResult Edit( string productTitle, float price ) {

			var model = ProductStaticRepository.Get( productTitle );

			model.Price = price;


			var hub = GlobalHost.ConnectionManager.GetHubContext<AuctionHub>();

			hub.Clients.Group( productTitle ).UpdatePrice( price ); //Обновляем цену у всех на странице продукта


			ProductStaticRepository.Save( model );

			return RedirectToAction( "Index" );
		}

	}
}
